#ifndef DRAW_H
#define DRAW_H

#include <QMainWindow>
#include <QTreeWidgetItem>
#include <QTableWidgetItem>
#include <QTextEdit>

#include <QGraphicsScene>
#include <QGraphicsSceneDragDropEvent>

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

QT_BEGIN_NAMESPACE
namespace Ui { class Draw; }
QT_END_NAMESPACE

class Draw;

/* ---------------------------------------------------------------------- */

class Source;
class Target;

class Source : public QGraphicsRectItem
{
public:
   Source ();
   int rel;
   QGraphicsLineItem * line;
   Target * target;

protected:
   QVariant itemChange (GraphicsItemChange change, const QVariant &value) override;
public:
   void updateLine ();
 };

class Target : public QGraphicsRectItem
{
public:
   Target ();
   int rel;
   Source * source;
protected:
   QVariant itemChange (GraphicsItemChange change, const QVariant &value) override;
};

/* ---------------------------------------------------------------------- */

class MyTreeItem : public QTreeWidgetItem
{
public:
    QGraphicsItem * graph;
};

/* ---------------------------------------------------------------------- */

class MyScene : public QGraphicsScene
{
public:
    MyScene (QObject * parent, Draw * window) : QGraphicsScene (parent), win (window) { }

private:
    Draw * win;

    void createSourceAndTarget (Source * & source, Target * & target);
    void place (QGraphicsItem * tem, QGraphicsItem * location, QPointF point);
    void placeAtPoint (QGraphicsItem * item, QPointF point);

    void moveUp (QGraphicsItem * item);
    void moveDown (QGraphicsItem * item);
    void linkItem (QGraphicsItem * item, QPointF point);

    QPointF startPos;

protected:
    void dragEnterEvent (QGraphicsSceneDragDropEvent * event) override;
    void dragMoveEvent (QGraphicsSceneDragDropEvent * event) override;
    void dropEvent (QGraphicsSceneDragDropEvent * event) override;

    void contextMenuEvent (QGraphicsSceneContextMenuEvent * event) override;

    void mousePressEvent (QGraphicsSceneMouseEvent * event) override;
    void mouseReleaseEvent (QGraphicsSceneMouseEvent * event) override;

    void mouseDoubleClickEvent (QGraphicsSceneMouseEvent  *event) override;

private :
    int relCnt = 0;

    // only for input/output
    QMap < int, int > renameMap;
    QMap < int, Source * > sourceMap;
    QMap < int, Target * > targetMap;
    QList < Source * > sourceList;
    QList < Target * > targetList;

public:
    void initInput ();
    int  renameRel (int orig_rel);
    void storeSource (Source * source);
    void storeTarget (Target * target);
    void completeInput ();
};

/* ---------------------------------------------------------------------- */

class Draw : public QMainWindow
{
    Q_OBJECT

public:
    Draw (QWidget *parent = nullptr);
    ~Draw ();
    void put (QString s);

private slots:
    void selectionChanged ();

    void on_tree_itemClicked (QTreeWidgetItem *item, int column);
    void on_table_itemChanged (QTableWidgetItem *item);


    void on_menuOpen_triggered ();
    void on_menuSave_triggered ();
    void on_menuQuit_triggered ();

    void on_menuCut_triggered ();
    void on_menuCopy_triggered ();
    void on_menuPaste_triggered ();

    void on_menuWebEngineView_triggered ();
    void on_menuWebKitView_triggered();
    void on_menuVisualizationView_triggered ();

    void on_menuQt3DView_triggered();

    void on_menuRun_triggered();

    void on_menuTree_triggered();

    void on_menuJavaScriptView_triggered();

public:
    Ui::Draw * ui;
    MyScene * scene;

    QMap < QGraphicsItem *, MyTreeItem * > treeMap;

    QGraphicsItem * prop_object = nullptr;

    void displayTree ();
    void displayBranch (QTreeWidgetItem * branch, QGraphicsItem * item);

    void displayProperties (QGraphicsItem * item);
    QTableWidgetItem * displayLine (QString name, QVariant value);
    void storeProperty (QGraphicsItem * item, QString name, QVariant value);

    void loadFile (QString fileName);
    void saveFile (QString fileName);
};

/* ---------------------------------------------------------------------- */

const QString shapeFormat = "application/x-shape";

Draw * getWin ();
MyScene *  getScene ();
QTreeWidget * getTree ();
QTableWidget * getProp ();
QTextEdit * getInfo ();
void put (QString s);

QString itemTypeName (QGraphicsItem * item);

QString colorToString (QColor c);
QString penToString (QPen p);
QString brushToString (QBrush b);

QColor stringToColor (QString name, QString default_name);

QString getString (QXmlStreamAttributes & attr, QString name);
int     getNumber (QXmlStreamAttributes & attr, QString name, int init = 0);
QColor  stringToColor (QXmlStreamAttributes & attr, QString name, QColor init = QColor ("yellow"));

/* ---------------------------------------------------------------------- */

#endif // DRAW_H
