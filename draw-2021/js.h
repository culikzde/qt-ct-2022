
/* js.h */

#ifndef JS_H
#define JS_H

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QAbstractGraphicsShapeItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>

#include <QTextEdit>
#include <QCompleter>

#include <QScriptValue>

/* ---------------------------------------------------------------------- */

#define COMPLETION

class JsEdit : public QTextEdit
{
   Q_OBJECT

   public:
      explicit JsEdit (QWidget * parent, QTreeWidget * tree_param, QTextEdit * info_param, QGraphicsScene * scene_param);

    // drag and drop
    protected:
        bool canInsertFromMimeData (const QMimeData * data) const override;
        void insertFromMimeData (const QMimeData * data) override;

    // text completion
    #ifdef COMPLETION
    protected:
       void keyPressEvent (QKeyEvent *e) override;
       void focusInEvent (QFocusEvent *e) override;

    private slots:
       void insertCompletion (const QString &completion);

    private:
       QCompleter * completer;

    private:
       QString textUnderCursor() const;
       void setCompleter (QCompleter * c);
       void setCompletion (QStringList c);
   #endif

   // Java Script
   private:
      QTreeWidget * tree;
      QTextEdit * info;
      QGraphicsScene * scene;

      void showItem (QTreeWidgetItem * above, QString name, QScriptValue value, int level);
      void showBranch (QTreeWidgetItem * above, QScriptValue branch, int level);
      void addToCompletion (QScriptValue branch);

      void execute (QScriptEngine & engine);

   public:
      void setTree (QTreeWidget * p_tree);
      void setInfo (QTextEdit * p_info);

   public slots:
      void debug ();
      void run ();
};

/* ---------------------------------------------------------------------- */

QString penToString (QPen p);
QString brushToString (QBrush b);

class JsShape : public QObject
{
   Q_OBJECT
   Q_CLASSINFO ("D-Bus Interface", "org.example.ShapeInterface")
   public:
      JsShape (QAbstractGraphicsShapeItem * shape_param) : shape (shape_param) { }

   private:
      QAbstractGraphicsShapeItem * shape;

   public:
      Q_PROPERTY (QString toolTip READ toolTip WRITE setToolTip)
      QString toolTip () { return shape->toolTip (); }
      void setToolTip (QString value) { return shape->setToolTip (value); }

      Q_PROPERTY (qreal x READ getX WRITE setX)
      qreal getX () { return shape->x (); }
      void setX (qreal value) { return shape->setX (value); }

      Q_PROPERTY (qreal y READ getY WRITE setY)
      qreal getY () { return shape->y (); }
      void setY (qreal value) { return shape->setY (value); }

      Q_PROPERTY (QColor pen READ getPen WRITE setPen)
      QColor getPen () { return shape->pen ().color(); }
      void setPen (QColor value) { shape->setPen (value); }

      Q_PROPERTY (QColor brush READ getBrush WRITE setBrush)
      QColor getBrush () { return shape->brush().color(); }
      void setBrush (QColor value) { shape->setBrush (value); }

      Q_PROPERTY (qreal width READ getWidth WRITE setWidth)
      Q_PROPERTY (qreal height READ getHeight WRITE setHeight)

      Q_INVOKABLE qreal getWidth ()
      {
          int w = 0;
          if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
          {
             w = e->rect().width();
          }
          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
             w  = e->rect().width();
          }
          return w;
      }

      Q_INVOKABLE qreal getHeight ()
      {
          int h = 0;
          if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
          {
             h = e->rect().height();
          }
          if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
          {
             h  = e->rect().width();
          }
          return h;
      }

      Q_INVOKABLE void setWidth (qreal value)
      {
            if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setWidth (value);
                e->setRect (r);
            }
            if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setWidth (value);
                e->setRect (r);
            }
      }

      Q_INVOKABLE void setHeight (qreal value)
      {
            if (QGraphicsRectItem * e = dynamic_cast < QGraphicsRectItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setHeight (value);
                e->setRect (r);
            }
            if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
            {
                QRectF r = e->rect ();
                r.setHeight (value);
                e->setRect (r);
            }
      }

      Q_PROPERTY (QString penName READ getPenName WRITE setPenName)
      Q_INVOKABLE QString getPenName () { return penToString (shape->pen ()); }
      Q_INVOKABLE void setPenName (QString value) { shape->setPen (QColor (value)); }

      Q_PROPERTY (QString brushName READ getBrushName WRITE setBrushName)
      Q_INVOKABLE QString getBrushName () { return brushToString (shape->brush()); }
      Q_INVOKABLE void setBrushName (QString value) { shape->setBrush (QColor (value)); }
};

/* ---------------------------------------------------------------------- */


// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all

#endif // JS_H
