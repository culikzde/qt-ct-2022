#include "toolbutton.h"
#include <QDrag>
#include <QMimeData>
#include <QMouseEvent>
#include <QFile>

void ToolButton::mousePressEvent (QMouseEvent * event)
{
   if (event->button() == Qt::LeftButton )
   {
      QMimeData * mimeData = new QMimeData;
      mimeData->setData (format, name.toLatin1());

      QDrag * drag = new QDrag (this);
      drag->setMimeData (mimeData);
      drag->setPixmap (icon.pixmap (24, 24));
      drag->setHotSpot (QPoint (-20, -16));

      Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
   }
}

ToolButton::ToolButton (QToolBar * parent, QString p_name, QString p_icon_name, QString p_format) :
   QToolButton (parent)
{
    name = p_name;
    format = p_format;

    if (p_icon_name == "")
       p_icon_name = ":/icons/" + p_name + ".svg";

    QFile file (p_icon_name);
    if (file.exists ())
    {
       icon = QIcon (p_icon_name);
       setIcon (icon);
    }

    setText (name);
    setToolTip (name);

    parent->addWidget (this);
}

void addToolButton (QToolBar * page, QString name, QString icon, QString format)
{
    new ToolButton (page, name, icon, format);
}

QStringList componentTools ();

void addToolButtons (QToolBar * page)
{
    addToolButton (page, "rectangle");
    addToolButton (page, "ellipse");
    addToolButton (page, "line");

    for (QString name : componentTools ())
        addToolButton (page, name);
}


