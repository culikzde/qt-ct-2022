#include "scene.h"
#include "toolbutton.h"
#include "area.h"
#include "io.h"
#include "draw.h"

Scene::Scene (DrawWindow * window) :
    win (window)
{
    connect (this, &QGraphicsScene::selectionChanged, this, &Scene::onSelectionChanged);
}

void Scene::onSelectionChanged ()
{
    QList<QGraphicsItem *> list = selectedItems ();
    if (list.count () == 1)
        win->showProperties (list [0]);
}

/*
bool Scene::event (QEvent * e)
{
    if (e->type () == QEvent::GraphicsSceneMove)
        win->showInfo ("MOVE");

    return QGraphicsScene::event (e);
}
*/

void Scene::dragEnterEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * data = event->mimeData();
    event->setAccepted (data->hasColor() || data->hasFormat (toolFormat));
}

void Scene::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
}

void Scene::dropEvent (QGraphicsSceneDragDropEvent *event)
{
    const QMimeData * data = event->mimeData();
    if (data->hasColor())
    {
        QColor color = data->colorData().value <QColor> ();
        QPointF pos = event->scenePos ();
        QGraphicsItem * item = itemAt (pos, QTransform ());
        if (item == nullptr)
        {
            QBrush brush = backgroundBrush();
            brush.setColor (color);
            setBackgroundBrush (brush);
        }
        else if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
            if (event->proposedAction() == Qt::CopyAction) // Ctrl Mouse
                shape->setPen (color);
            else
                shape->setBrush (color);
        }
        else if (auto line = dynamic_cast < QGraphicsLineItem * > (item))
        {
            QPen pen = line->pen ();
            pen.setColor (color);
            line->setPen (pen);
        }
    }
    else if (data->hasFormat (toolFormat))
    {
        QString tool = data->data (toolFormat);
        QGraphicsItem * item = itemFromTool (tool);

        QPointF p = event->scenePos ();
        QGraphicsItem * target = itemAt (p, QTransform ());

        if (target == nullptr)
        {
           addItem (item);
           item->setPos (p);
        }
        else
        {
           item->setParentItem (target);
           p = target->mapFromScene (p);
           item->setPos (p);
        }

        // refreshTree ();
    }
}

QGraphicsItem * itemFromTool (QString tool)
{
    QGraphicsItem * result = nullptr;

    /*
    if (tool == "line")
    {
        result = new Source;
    }
    else
    */
    {
        TypeDesc * desc = findDescription (tool);
        if (desc != nullptr)
           result = desc->createItem ();
    }

    /*
    if (result != nullptr)
    {
       result->setFlags (QGraphicsItem::ItemIsMovable  | QGraphicsEllipseItem::ItemIsSelectable);
    }
    */
    return result;
}

