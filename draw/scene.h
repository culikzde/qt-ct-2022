#ifndef SCENE_H
#define SCENE_H

#include "precompiled.h"

class DrawWindow;

class Scene : public QGraphicsScene
{
public:
    Scene (DrawWindow * window);

protected:
    void dragEnterEvent (QGraphicsSceneDragDropEvent *event) override;
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    void dropEvent (QGraphicsSceneDragDropEvent *event) override;

    // bool event (QEvent * e) override;
    void onSelectionChanged ();

private:
    DrawWindow * win;
};

QGraphicsItem * itemFromTool (QString tool);

#endif // SCENE_H
