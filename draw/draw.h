#ifndef DRAWWINDOW_H
#define DRAWWINDOW_H

#include "precompiled.h"

QT_BEGIN_NAMESPACE
namespace Ui { class DrawWindow; }
QT_END_NAMESPACE

class Scene;

class DrawWindow : public QMainWindow
{
    Q_OBJECT

public:
    DrawWindow(QWidget *parent = nullptr);
    ~DrawWindow();

    void openFile (QString fileName, bool json = false);
    void saveFile (QString fileName, bool json = false);

    void showInfo (QString txt);
    void showProperties (QGraphicsItem * item);
    void refreshTree();

private slots:
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionQuit_triggered();

    void on_actionShowTree_triggered();
    void on_tree_itemClicked(QTreeWidgetItem *item, int column);

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

private:
    Ui::DrawWindow *ui;
    Scene * scene;
};

#endif // DRAWWINDOW_H
