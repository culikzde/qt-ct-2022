#include "table.h"
#include "io.h"
#include "draw.h"

Table::Table (QWidget * parent) :
  QTableWidget (parent)
{
    setColumnCount (2);
    setHorizontalHeaderLabels (QStringList() << "Name" << "Value");
    horizontalHeader()->setStretchLastSection (true);

    setItemDelegate (new CustomDelegate (this));

    connect (this, &QTableWidget::itemChanged, this, &Table::onItemChanged);
}

void Table::put (QString txt)
{
   if (win != nullptr)
      win->showInfo (txt);
}

void Table::addField (FieldDesc * field)
{
   int inx = rowCount ();
   setRowCount (inx+1);

   QTableWidgetItem * temp = new QTableWidgetItem;
   temp->setText (field->name);
   setItem (inx, 0, temp);

   TableItem * node = new TableItem;
   setItem (inx, 1, node);

   node->graph_item = graph_item;
   node->field = field;

   QString text = field->readText (graph_item);
   node->setText (text);

   QVariant data = field->decoration (graph_item);
   if (data.type () != QVariant::Invalid)
       node->setData (Qt::DecorationRole, data);
}

void Table::onItemChanged (QTableWidgetItem * node0)
{
   TableItem * node = dynamic_cast < TableItem *> (node0);
   if (node != nullptr && node->graph_item != nullptr && node->field != nullptr)
   {
      FieldDesc * field = node->field;

      QString s = node->text ();
      put ("Changing " + field->name + " to " + s);

      field->write (node->graph_item, s);

      // update decoration
      QVariant data = field->decoration (graph_item);
      if (data.type () != QVariant::Invalid)
          node->setData (Qt::DecorationRole, data);
   }
}

void Table::display (QGraphicsItem * item)
{
    setRowCount (0);
    graph_item = item;

    TypeDesc * desc = typeDescription (item);

    for (FieldDesc * field : desc->fields)
         addField (field);
}

/* ---------------------------------------------------------------------- */

QWidget * CustomDelegate::createEditor (QWidget * parent,
                                        const QStyleOptionViewItem & option,
                                        const QModelIndex & index) const
{
   CustomEditor * editor = new CustomEditor (parent);

   if (index.column () == 1)
   {
      editor->table_item = dynamic_cast <TableItem * > (table->item (index.row(), index.column ()));

      FieldDesc * field = editor->table_item->field;
      field->setupEditor (editor);

      QHBoxLayout * layout = new QHBoxLayout (editor);
      layout->setMargin (0);
      layout->setSpacing (0);
      editor->setLayout (layout);

      if (editor->enable_list)
      {
         editor->combo_box = new QComboBox (editor);
         layout->addWidget (editor->combo_box);

         int cnt = editor->list_values.count ();
         for (int inx = 0; inx < cnt; inx++)
            editor->combo_box->addItem (editor->list_values [inx]);
         if (cnt > 0)
            editor->combo_box->setCurrentIndex (0);
      }

      if (editor->enable_checkbox)
      {
         editor->check_box = new QCheckBox (editor);
         layout->addWidget (editor->check_box);
      }

      if (editor->enable_text)
      {
         editor->line_edit = new QLineEdit (editor);
         layout->addWidget (editor->line_edit);
      }

      if (editor->enable_numeric)
      {
          editor->numeric_edit = new QSpinBox (editor);
          editor->numeric_edit->setMinimum (-10000);
          editor->numeric_edit->setMaximum (10000);
          layout->addWidget (editor->numeric_edit);
      }

      if (editor->enable_real)
      {
         editor->real_edit = new QDoubleSpinBox (editor);
         editor->real_edit->setMinimum (-10000);
         editor->real_edit->setMaximum (10000);
         layout->addWidget (editor->real_edit);
      }

      if (editor->enable_dialog)
      {
         editor->button = new QPushButton (editor);
         editor->button->setText ("...");
         editor->button->setMaximumWidth (32);
         layout->addWidget (editor->button);

         connect (editor->button, &QPushButton::clicked, editor, &CustomEditor::onDialogClick);
      }
   }

   return editor;
}

void CustomDelegate::setEditorData (QWidget * param_editor,
                                    const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);

   QVariant value = index.data (Qt::EditRole);

   if (editor->check_box != nullptr)
   {
      editor->check_box->setChecked (value.toBool ());
   }

   if (editor->line_edit != nullptr)
   {
      editor->line_edit->setText (value.toString ());
   }

   if (editor->numeric_edit != nullptr)
   {
      editor->numeric_edit->setValue (value.toInt ());
   }

   if (editor->real_edit != nullptr)
   {
      editor->real_edit->setValue (value.toDouble ());
   }
}

void CustomDelegate::setModelData (QWidget * param_editor,
                                   QAbstractItemModel * model,
                                   const QModelIndex & index) const
{
   CustomEditor * editor = dynamic_cast < CustomEditor * > (param_editor);

   if (editor->check_box != nullptr)
   {
      bool val = editor->check_box->isChecked ();
      model->setData (index, val);
   }

   if (editor->line_edit != nullptr)
   {
      QString txt = editor->line_edit->text ();
      model->setData (index, txt);
   }

   if (editor->numeric_edit != nullptr)
   {
      int val = editor->numeric_edit->value ();
      model->setData (index, val);
   }

   if (editor->real_edit != nullptr)
   {
      double val = editor->real_edit->value ();
      model->setData (index, val);
   }
}

/*
void CustomDelegate::updateEditorGeometry (QWidget * param_editor,
                                           const QStyleOptionViewItem & option,
                                           const QModelIndex &index) const
{
    QStyledItemDelegate::updateEditorGeometry (param_editor, option, index);
    // param_editor->setGeometry (param_editor->geometry().adjusted(0, 0, -1, -1));
}

QSize CustomDelegate::sizeHint (const QStyleOptionViewItem & option,
                               const QModelIndex & index) const
{
    return QStyledItemDelegate::sizeHint (option, index) + QSize (16, 16);
}
*/

void CustomEditor::onDialogClick ()
{
    QVariant value = table_item->data (Qt::EditRole);
    QColor color = value.value <QColor> ();
    color = QColorDialog::getColor (color);
    if (color.isValid ())
    {
        table_item->setData (Qt::EditRole, color);
    }
}
