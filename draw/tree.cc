#include "tree.h"
#include "io.h"

Tree::Tree (QWidget * parent) :
    QTreeWidget (parent)
{
    setSelectionMode (QAbstractItemView::SingleSelection);
    // setDragEnabled (true);
    // viewport()->setAcceptDrops(true);
    // setDropIndicatorShown (true);

    // setDragDropMode (QAbstractItemView::InternalMove);
    // setDragDropMode (QAbstractItemView::DragDrop);
    setDragDropMode (QAbstractItemView::DropOnly);
}

QStringList Tree::mimeTypes() const
{
   QStringList types;
   types << "application/x-color";
   return types;
}

/*
Qt::DropActions Tree::supportedDropActions () const
{
     return Qt::CopyAction | Qt::MoveAction | Qt::LinkAction;
}
*/

bool Tree::dropMimeData (QTreeWidgetItem * orig_node, int index, const QMimeData *data, Qt::DropAction action)
{
    bool ok = false;
    if (data->hasColor ())
    {
       QColor color = data->colorData().value<QColor>();
       TreeNode * node = dynamic_cast < TreeNode * > (orig_node);
       if (node != nullptr && node->graph_item != nullptr)
       {
           // node->setBackground (0, color);
           TypeDesc * type_desc = typeDescription (node->graph_item);
           if (type_desc != nullptr)
           {
              QString field_name = "brush";
              if (action == Qt::CopyAction) // Ctrl Mouse
                 field_name = "pen" ;
              FieldDesc * field = type_desc->findField (field_name);
              if (field != nullptr)
                  field->write (node->graph_item, color);
           }
       }
    }
    return ok;
}


