#ifndef TABLE_H
#define TABLE_H

#include "precompiled.h"
#include "io.h"

/* Property Table */

class DrawWindow;

class Table : public QTableWidget
{
public:
    Table (QWidget * parent = nullptr);
    void display (QGraphicsItem * item);

    void setWin (DrawWindow * window) { win = window; }
    void put (QString txt);
private:
    DrawWindow * win = nullptr;
    QGraphicsItem * graph_item = nullptr;
    TypeDesc * type_desc = nullptr;
    void addField (FieldDesc * field);
    void onItemChanged (QTableWidgetItem * node);
};

/* Custom Editor */

class CustomDelegate : public QStyledItemDelegate
{
   public:
      virtual QWidget * createEditor
         (QWidget * parent,
          const QStyleOptionViewItem & option,
          const QModelIndex & index) const override;

      virtual void setEditorData
         (QWidget * param_editor,
          const QModelIndex & index) const override;

      virtual void setModelData
         (QWidget * param_editor,
          QAbstractItemModel * model,
          const QModelIndex & index) const override;

    /*
    virtual void updateEditorGeometry
       (QWidget * param_editor,
        const QStyleOptionViewItem & option,
        const QModelIndex &index) const override;

    virtual QSize sizeHint
       (const QStyleOptionViewItem & option,
        const QModelIndex & index) const override;
    */

    Table * table;
    CustomDelegate (Table * p_table) : table (p_table) { }
};

#endif // TABLE_H
