#include "precompiled.h"
#include "draw.h"
#include "ui_draw.h"

#include "colorbutton.h"
#include "toolbutton.h"
#include "scene.h"
#include "io.h"

DrawWindow::DrawWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::DrawWindow)
{
    ui->setupUi(this);

    ui->vsplitter->setStretchFactor (0, 4);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->splitter->setStretchFactor (0, 1);
    ui->splitter->setStretchFactor (1, 3);
    ui->splitter->setStretchFactor (2, 1);

    /* tree */

    // ui->tree->setWin (this);

    // ui->tree->setHeaderLabels (QStringList() << "name" << "size" << "data");
    ui->tree->header()->hide();

    /* property table */

    ui->prop->setWin (this);

    /* color buttons */

    ui->palette->clear ();
    QToolBar * colorBar = new QToolBar (this);
    ui->palette->addTab (colorBar, "Colors");

    QStringList list;
    list << "red" << "green" << "blue" << "orange" << "yellow" << "lime" << "cornflowerblue";

    /* tool buttons */

    for (QString name : list)
        new ColorButton (colorBar, name);

    QToolBar * shapeToolBar = new QToolBar (this);
    ui->palette->addTab (shapeToolBar, "Shapes");
    addToolButtons (shapeToolBar);

    /* graphics scene */

    scene = new Scene (this);
    ui->graphicsView->setScene(scene);

    /* backround */

    const int n = 16;
    QBitmap texture (n, n);
    texture.clear ();

    QPainter painter (&texture);
    painter.drawLine (0, 0, n-1, 0);
    painter.drawLine (0, 0, 0, n-1);
    painter.end ();

    QBrush brush ("cornflowerblue");
    brush.setTexture (texture);
    scene->setBackgroundBrush (brush);

    /* graphics items */

    QGraphicsLineItem * line = scene->addLine (0, 0, 200, 100, QPen (QColor ("red"), 7) );
    line->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);

    QGraphicsRectItem * r = new QGraphicsRectItem;
    r->setPos (100, 100);
    r->setRect (0, 0, 200, 100);
    r->setPen (QColor ("blue"));
    r->setBrush (QColor ("yellow"));
    r->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
    scene->addItem (r);

    for (int i = 1; i <= 2; i++)
    {
        QGraphicsEllipseItem * e = new QGraphicsEllipseItem;
        e->setPos (40 + 80 *(i-1), 30);
        e->setRect (0, 0, 40, 40);
        e->setPen (QColor ("blue"));
        e->setBrush (QColor ("cornflowerblue"));
        e->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
        e->setParentItem (r);
    }

    refreshTree ();
}

DrawWindow::~DrawWindow()
{
    delete ui;
}

void DrawWindow::showInfo (QString txt)
{
    ui->info->append (txt);
}

void DrawWindow::showProperties (QGraphicsItem * item)
{
    ui->prop->display (item);
}

/* property table */

QTreeWidgetItem * displayBranch (QGraphicsItem * item)
{
    TreeNode * node = new TreeNode;
    node->graph_item = item;

    QString name = "item";
    if (auto shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        node->setForeground (0, shape->pen().color());
        // node->setBackground(0, shape->brush().color());
        node->setData (0, Qt::DecorationRole, shape->brush().color());
        if (dynamic_cast < QGraphicsRectItem * > (shape))
            name = "rectangle";
        else if (dynamic_cast < QGraphicsEllipseItem * > (shape))
            name = "ellipse";
    }
    else if (auto line = dynamic_cast < QGraphicsLineItem * > (item))
    {
        name = "line";
        node->setData (0, Qt::DecorationRole, line->pen().color());
    }
    node->setText (0, name);

    for (QGraphicsItem * inner_item : item->childItems())
    {
         QTreeWidgetItem * inner_node = displayBranch (inner_item);
         node->addChild (inner_node);
    }

    return node;
}

/* tree */

void DrawWindow::refreshTree ()
{
    ui->tree->clear ();
    for (QGraphicsItem * item : scene->items())
    {
        if (item->parentItem() == nullptr)
        {
           QTreeWidgetItem * node = displayBranch (item);
           ui->tree->addTopLevelItem (node);
        }
    }
    ui->tree->expandAll();
}

void DrawWindow::on_actionShowTree_triggered()
{
    refreshTree ();

    /*
    QTreeWidgetItem * branch = new QTreeWidgetItem;
    branch->setText (0, "branch");
    branch->setForeground (0, QColor ("brown"));
    ui->tree->addTopLevelItem (branch);

    for (int i = 1; i <= 3; i++)
    {
        QTreeWidgetItem * node = new QTreeWidgetItem;
        node->setText (0, "list " + QString::number(i) );
        node->setForeground (0, QColor ("lime"));
        branch->addChild (node);
    }

    branch->setExpanded (true);
    */
}

void DrawWindow::on_tree_itemClicked (QTreeWidgetItem *item, int column)
{
    TreeNode * node = dynamic_cast <TreeNode *> (item);
    if (node != nullptr)
    {
        for (QGraphicsItem * item : scene->selectedItems())
            item->setSelected (false);

        node->graph_item->setSelected (true);

        showProperties (node->graph_item);
    }
}

/* input / output */

void DrawWindow::openFile (QString fileName, bool json)
{
     QFile f (fileName);
     if (f.open (QFile::ReadOnly))
     {
         if (json)
         {
             QByteArray code = f.readAll ();
             readJson (code, scene);
         }
         else
         {
            QXmlStreamReader r (&f);
            readXml (r, scene);
         }
         refreshTree ();
     }
     else
     {
        QMessageBox::warning (NULL, "Open File Error", "Cannot read file: " + fileName);
     }
}

void DrawWindow::saveFile (QString fileName, bool json)
{
    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
        if (json)
        {
            QByteArray code = writeJson (scene);
            f.write (code);
        }
        else
        {
           QXmlStreamWriter w (&f);
           writeXml (w, scene);
        }
    }
    else
    {
       QMessageBox::warning (NULL, "Save File Error", "Cannot write file: " + fileName);
    }
}

const QString dir = QString ();
const QString filter = "Json files (*.json);;XML files (*.xml)";
const QString jsonFilter = "Json files";

void DrawWindow::on_actionOpen_triggered()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName (this, "Open file", dir, filter, &selectedFilter);
    if (fileName != "")
        openFile (fileName, fileName.endsWith (".json"));
}

void DrawWindow::on_actionSave_triggered()
{
    QString fileName = QFileDialog::getSaveFileName (this, "Save file");
    if (fileName != "")
        saveFile (fileName, fileName.endsWith (".json"));
}

void DrawWindow::on_actionQuit_triggered()
{
    close ();
}

int main (int argc, char *argv[])
{
    QApplication a (argc, argv);
    initColorMap ();
    DrawWindow w;
    w.show();
    return a.exec();
}


void DrawWindow::on_actionCopy_triggered()
{
    QString code = "";
    QXmlStreamWriter writer (& code);
    writeBegin (writer);

    for (QGraphicsItem * item : scene->selectedItems ())
    {
        writeXmlItem (writer, item);
    }

    writeEnd (writer);

    QMimeData * data = new QMimeData;
    data->setData (shapeFormat, code.toLatin1 ());

    QClipboard * clip = QApplication::clipboard ();
    clip->setMimeData (data);
}


void DrawWindow::on_actionPaste_triggered()
{
    QClipboard * clip = QApplication::clipboard ();
    const QMimeData * data = clip->mimeData ();
    if (data->hasFormat (shapeFormat))
    {
        QString code = data->data (shapeFormat);
        QXmlStreamReader reader (code);
        readXml (reader, scene);

    }
    else if (data->hasText ())
    {
       QString name = data->text ();
       TypeDesc * desc = findDescription (name);
       if (desc != nullptr)
       {
           QGraphicsItem * item = desc->createItem();
           if (item != nullptr)
           {
              scene->addItem (item);
              refreshTree ();
           }
       }
    }
}

