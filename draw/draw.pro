QT       += core gui widgets

CONFIG   += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += \
    area.cc \
    colorbutton.cc \
    io.cc \
    table.cc \
    toolbutton.cc \
    draw.cc \
    scene.cc \
    tree.cc

HEADERS += \
    area.h \
    io.h \
    precompiled.h \
    colorbutton.h \
    io.h \
    table.h \
    tree.h \
    toolbutton.h \
    draw.h \
    scene.h

FORMS += draw.ui

RESOURCES += resources.qrc

