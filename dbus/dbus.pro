QT += core gui widgets qml
# dnf install qt5-qtquickcontrols2-devel

CONFIG += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += box.cc script.cc

HEADERS += box.h script.h precompiled.h

FORMS += script.ui

CONFIG += DBUS

DBUS {
    QT += dbus
    DEFINES += DBUS
}
