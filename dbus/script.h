#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "precompiled.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionRun_triggered();

    void on_actionQuit_triggered();

private:
    Ui::MainWindow *ui;

    QGraphicsView * graphicsView;
    QGraphicsScene * scene;

    QTextEdit * output;
};

#endif // MAINWINDOW_H
