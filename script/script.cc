#include "script.h"
#include "ui_script.h"
#include "box.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->splitter->setStretchFactor(0, 2);
    ui->splitter->setStretchFactor(1, 1);

    ui->tabs->clear ();

    graphicsView = new QGraphicsView (this);
    scene = new QGraphicsScene;
    graphicsView->setScene (scene);
    ui->tabs->addTab (graphicsView, "Graphics");

    output = new QTextEdit;
    ui->tabs->addTab (output, "Output");

    for (int i = 1; i <= 2; i++)
    {
       Box * box = new Box;
       box->name = "box" + QString::number (i);
       box->move (20*i, 10*i);
       box->setToolTip (box->name);
       scene->addItem (box);
    }

    /*
    QString code = R"CODE(
       output.append ("some text");
       for (i = 1; i <= 3; i++)
       {
          output.append (i);
       }
    )CODE";
    */

    QString code = R"CODE(
       box1.setColor ("lime");
       output.append ("cmd 1");
       box1.setBorder ("orange", 3);
       output.append ("cmd 2");
       box2.move (100, 20);
       output.append ("cmd 3");
    )CODE";

    QStringList lines = code.split ("\n");

    while (lines.size() != 0 and lines[0].simplified().length() == 0)
        lines.removeFirst ();

    QString txt = lines [0];
    int len = txt.length ();
    int spaces = 0;
    while (spaces < len && txt[spaces] == ' ') spaces ++;

    code = "";
    for (QString line : lines)
    {
        QString txt = line;
        int len = txt.length ();
        int inx = 0;
        while (inx < len && inx < spaces && txt [inx] == ' ') inx ++;
        txt = txt.right (len - inx);
        code = code + txt + "\n";
    }

    ui->input->setText (code);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionRun_triggered()
{
    // Fedora:         dnf install qt5-qtquickcontrols2-devel
    // .pro file:      QT += core gui widgets qml
    // precompiled.h : #include <QtQml>
    // menu Build / Rebuid


    /*
    QJSEngine engine;
    QJSValue value = engine.evaluate ("1 + 2");
    output->append (value.toString());
    ui->tabs->setCurrentWidget (output);
    */

    /*
    QJSEngine engine;
    QJSValue proxy = engine.newQObject (output);
    engine.globalObject().setProperty ("output", proxy);

    QString code = ui->input->toPlainText ();
    engine.evaluate (code);
    ui->tabs->setCurrentWidget (output);
    */

    QQmlEngine engine; // setObjectOwnership
    QJSValue global = engine.globalObject();
    global.setProperty ("output", engine.newQObject (output));

    for (QGraphicsItem * item : scene->items ())
        if (Box * box = dynamic_cast <Box *> (item))
        {
            global.setProperty (box->name, engine.newQObject (box));
            engine.setObjectOwnership (box, QQmlEngine::CppOwnership);
        }

    QString code = ui->input->toPlainText ();
    engine.evaluate (code);
    // ui->tabs->setCurrentWidget (output);

}

void MainWindow::on_actionQuit_triggered()
{
    close ();
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}





