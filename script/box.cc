#include "box.h"

Box::Box()
{
   setRect (0, 0, 80, 40);
   setPen  (QColor ("blue"));
   setBrush (QColor ("yellow"));
   setFlags (QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable);
}

void Box::move (int x, int y)
{
    QPointF p = pos () + QPointF (x, y);
    setPos (p);
}

void Box::setColor (QString color)
{
    setBrush (QColor (color));
}

void Box::setBorder (QString color, int width)
{
    setPen (QPen (QColor (color), width));
}


