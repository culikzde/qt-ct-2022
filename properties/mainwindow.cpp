#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMetaProperty>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    displayObject (this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::displayLine (QString name, QString value)
{
    int cnt = ui->tableWidget->rowCount();
    ui->tableWidget->setRowCount (cnt+1);

    QTableWidgetItem * item = new QTableWidgetItem;
    item->setText (name);
    ui->tableWidget->setItem (cnt, 0, item);

    item = new QTableWidgetItem;
    item->setText (value);
    ui->tableWidget->setItem (cnt, 1, item);
}

void MainWindow::displayObject (QObject * obj)
{
    global_obj = obj;
    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setRowCount(0);

    const QMetaObject * cls = obj->metaObject();
    // displayLine ("class", cls->className ());

    // #include <QMetaProperty>
    int cnt = cls->propertyCount();
    for (int inx = 0; inx < cnt; inx++)
    {
        QMetaProperty p = cls->property (inx);
        QVariant value = p.read (obj);
        QString n = p.name ();
        QString t = p.typeName();
        displayLine (n + " : " + t, value.toString());
    }
}

void MainWindow::on_tableWidget_cellChanged(int row, int column)
{
    if (column == 1)
    {
        QString txt = ui->tableWidget->item(row, column)->text ();

        QObject * obj = global_obj;
        const QMetaObject * cls = obj->metaObject();
        int inx = row;
        QMetaProperty p = cls->property (inx);

        QVariant value = txt;
        if (p.type() == QVariant::Bool)
        {
            if (txt == "true")
                value = true;
            else
                value = false;
        }
        else if (p.type() == QVariant::Int)
        {
            value = QVariant (txt.toInt());
        }

        p.write (obj, value);
    }
}


void MainWindow::on_tableWidget_cellClicked(int row, int column)
{

}


