#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void displayObject(QObject *obj);
    void displayLine(QString name, QString value);
private slots:
    void on_tableWidget_cellClicked(int row, int column);

    void on_tableWidget_cellChanged(int row, int column);

private:
    Ui::MainWindow *ui;
    QObject * global_obj = nullptr;
};
#endif // MAINWINDOW_H
