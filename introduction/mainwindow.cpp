﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    :
    QMainWindow (parent),
    ui (new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::print (QString s)
{
    ui->textEdit->append (s);
}

void MainWindow::setColor (QColor c)
{
    QTextCharFormat fmt = ui->textEdit->currentCharFormat();
    fmt.setForeground (c);
    ui->textEdit->setCurrentCharFormat (fmt);
}

void MainWindow::on_pushButton_clicked()
{
   print ("");

   setColor ("blue");
   QString s = ui->lineEdit->text();
   print ("line edit ... " + s);

   setColor ( QColor::fromRgb (255, 0, 0) );

   bool b = ui->checkBox->isChecked();
   if (b)
       print ("check box ... checked");
   else
       print ("check box ... unchecked");

   QTextCharFormat fmt = ui->textEdit->currentCharFormat();
   fmt.setToolTip ("vysledek");
   ui->textEdit->setCurrentCharFormat (fmt);

   setColor ( QColor ("lime") );
   int n = ui->spinBox->value();
   print ("spin box ... " + QString::number (n)) ;
}
