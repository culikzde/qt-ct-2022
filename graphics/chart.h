#ifndef CHART_H
#define CHART_H

#include <QWidget>
#include <QGraphicsScene>

QWidget * chartExample (QGraphicsScene * scene);
QWidget * pieChartExample (QGraphicsScene * scene);

#endif // CHART_H
