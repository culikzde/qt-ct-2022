#ifndef JSONIO_H
#define JSONIO_H

#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

#include <QGraphicsScene>
#include <QGraphicsItem>

void readJson (QByteArray code, QGraphicsScene * scene,  QGraphicsItem * target = nullptr);
QByteArray writeJson (QGraphicsScene * scene);

#endif // JSONIO_H
