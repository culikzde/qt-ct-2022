#ifndef SCENE_H
#define SCENE_H

#include "win.h"

#include <QGraphicsView>
#include <QGraphicsScene>

/*
class View : public QGraphicsView
{
public:
    View (QWidget * parent = nullptr);
    ~ View ();
};
*/

class Scene : public QGraphicsScene
{
private:
    Win * win;

public:
    Scene ();
    void setWin (Win * p_win) { win = p_win; }

// QGraphicsScene interface
protected:
    virtual void dragEnterEvent (QGraphicsSceneDragDropEvent * event) override;
    virtual void dragMoveEvent(QGraphicsSceneDragDropEvent *event) override;
    virtual void dropEvent (QGraphicsSceneDragDropEvent * event) override;

private:
    void onSelectionChanged ();
};

#endif // SCENE_H
