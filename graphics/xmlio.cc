#include "xmlio.h"
#include "io.h"

#include <QAbstractGraphicsShapeItem>
#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>

/* ---------------------------------------------------------------------- */

QString getString (QXmlStreamAttributes & a, QString name)
{
    QString result = "";
    if (a.hasAttribute (name))
    {
        result = a.value(name).toString();
    }
    return result;
}

int getNumber (QXmlStreamAttributes & a, QString name, int init = 0)
{
    int result = init;
    if (a.hasAttribute (name))
    {
        bool ok;
        int val = a.value(name).toInt (&ok);
        if (ok)
            result = val;
    }
    return result;
}

QColor getColor (QXmlStreamAttributes & a, QString name, QColor init = QColor ("yellow"))
{
    QColor result = init;
    if (a.hasAttribute (name))
    {
        QString s = a.value(name).toString ();
        result = QColor (s);
    }
    return result;
}

/* ---------------------------------------------------------------------- */

QGraphicsItem * readItem (QXmlStreamReader & r)
{
    QGraphicsItem * item = nullptr;

    if (r.isStartElement())
    {
        QString type = r.name().toString();
        item = createItem (type);
        setupItem (item);

        QXmlStreamAttributes a = r.attributes();

        QString name = getString (a, "name");
        item->setToolTip(name);

        int x = getNumber (a, "x");
        int y = getNumber (a, "y");
        item->setPos (x, y);

        if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
        {
           QColor c = getColor(a, "pen", QColor ("red"));
           QColor d = getColor(a, "brush", QColor ("yellow"));

           shape->setPen (c);
           shape->setBrush (d);

           if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
           {
               int w = getNumber (a, "width", 100);
               int h = getNumber (a, "height", 80);
               r->setRect (0, 0, w, h);
           }

           if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
           {
               int w = getNumber (a, "width", 100);
               int h = getNumber (a, "height", 80);
               e->setRect (0, 0, w, h);
           }
        }

        if (QGraphicsLineItem * t = dynamic_cast < QGraphicsLineItem * > (item))
        {
            int w = getNumber (a, "width", 100);
            int h = getNumber (a, "height", 80);
            t->setLine (0, 0, w, h);

            QColor c = getColor (a, "pen", QColor ("blue"));
            t->setPen (c);
        }
    }

    return item;
}

void readXml (QXmlStreamReader & r, QGraphicsScene * scene, QGraphicsItem * target)
{
    while (! r.atEnd())
    {
        if (r.isStartElement())
        {
            QString type = r.name().toString();
            if (type != "data") // top level element
            {
                QGraphicsItem * item = readItem (r);
                if (item != nullptr)
                {
                    // add item
                    if (target != nullptr)
                       item->setParentItem (target);
                    else
                       scene->addItem (item);

                    // change target
                    target = item;
                }
            }
        }
        else if (r.isEndElement())
        {
            if (target != nullptr)
                target = target->parentItem ();
        }
        r.readNext();
    }
}

/* ---------------------------------------------------------------------- */

void writeItem (QXmlStreamWriter & w, QGraphicsItem * item)
{
    w.writeStartElement (itemType (item));
    w.writeAttribute ("name", item->toolTip());

    w.writeAttribute ("x", QString::number ( item->x() ));
    w.writeAttribute ("y", QString::number ( item->y() ));

    if (QAbstractGraphicsShapeItem * shape = dynamic_cast < QAbstractGraphicsShapeItem * > (item))
    {
        w.writeAttribute ("pen", penToString (shape->pen()));
        w.writeAttribute ("brush", brushToString (shape->brush()));

        if (QGraphicsRectItem * r = dynamic_cast < QGraphicsRectItem * > (shape))
        {
           w.writeAttribute ("width", QString::number ( r->rect().width() ));
           w.writeAttribute ("height", QString::number ( r->rect().height() ));
        }

        if (QGraphicsEllipseItem * e = dynamic_cast < QGraphicsEllipseItem * > (shape))
        {
           w.writeAttribute ("width", QString::number ( e->rect().width() ));
           w.writeAttribute ("height", QString::number ( e->rect().height() ));
        }
    }

    if (QGraphicsLineItem * e = dynamic_cast < QGraphicsLineItem * > (item))
    {
       w.writeAttribute ("pen", penToString (e->pen()));
       w.writeAttribute ("width", QString::number ( e->line().dx() ));
       w.writeAttribute ("height", QString::number ( e->line().dy() ));
    }

    for (QGraphicsItem * t : item->childItems ())
        writeItem (w, t);

    w.writeEndElement();
}

void writeBegin (QXmlStreamWriter & w)
{
    w.setAutoFormatting (true);
    w.writeStartDocument ();
    w.writeStartElement ("data");
}

void writeEnd (QXmlStreamWriter & w)
{
    w.writeEndElement(); // end of data
    w.writeEndDocument();
}

void writeXml (QXmlStreamWriter & w, QGraphicsScene * scene)
{
    writeBegin (w);

    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
       if (item->parentItem() == nullptr)
          writeItem (w, item);
    }

    writeEnd (w);
}

/* ---------------------------------------------------------------------- */
