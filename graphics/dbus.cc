
/* dbus.cc */

#include "dbus.h"

#include <QDBusConnection>

#include <iostream>
using namespace std;

Receiver::Receiver (QWidget * parent, QTextEdit * info_param) :
    QObject (parent),
    info (info_param)
{
    QDBusConnection bus = QDBusConnection::sessionBus();
    if (bus.isConnected ())
        if (bus.registerService ("org.example.receiver"))
            if (bus.registerObject ("/", this, QDBusConnection::ExportAllSlots))
                info->append ("DBus Receiver ready");
}

Receiver::~Receiver()
{
    // info->append ("DBus Receiver finished");
    cout << "DBus Receiver finished" << endl;
}

void Receiver::hello (QString s)
{
    info->append ("Receiver - method hello : " + s);
}

/* ---------------------------------------------------------------------- */

/*
  yum install qt-qdbusviewer
  qdbusviewer, vyhledat org.example.receiver

  nebo

  yum install qt5-qdbusviewer
  qdbusviewer-qt5
*/

// kate: indent-width 1; show-tabs true; replace-tabs true; remove-trailing-spaces all
