
#include "visual.h"

#include <QtDataVisualization>
using namespace QtDataVisualization;

QWidget * dataVisualizationExample ()
{
    Q3DBars * bars = new Q3DBars;
    // bars->setFlags (bars.flags() ^ Qt::FramelessWindowHint);
    bars->rowAxis()->setRange(0, 4);
    bars->columnAxis()->setRange(0, 4);
    QBar3DSeries * series = new QBar3DSeries;
    QBarDataRow * data = new QBarDataRow;
    *data << 1.0f << 3.0f << 7.5f << 5.0f << 2.2f;
    series->dataProxy()->addRow(data);
    bars->addSeries(series);
    return QWidget::createWindowContainer (bars);
}
