#ifndef TREE_H
#define TREE_H

#include "win.h"

#include <QTreeWidget>
#include <QTreeWidgetItem>

#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QMimeData>

class TreeNode;

class Tree : public QTreeWidget
{
    private:
        QGraphicsScene * scene;
        Win * win;

    public:
        Tree (QWidget * parent = nullptr);

        void setScene (QGraphicsScene * p_scene) { scene = p_scene; }
        void setWin (Win * p_win) { win = p_win; }

        void displayTree ();
        void renameTreeItem (QGraphicsItem * item, QString name);
        void addTreeItem (QGraphicsItem * item);

    private:
        TreeNode * findTreeItem (QGraphicsItem * item);
        void displayBranch (QTreeWidgetItem * target, QGraphicsItem * item);

    protected:
        virtual QStringList mimeTypes () const override;
        // virtual QMimeData * mimeData (const QList<QTreeWidgetItem *> items) const override;
        virtual bool dropMimeData (QTreeWidgetItem * parent, int index, const QMimeData * data, Qt::DropAction action) override;
        virtual Qt::DropActions supportedDropActions () const override;
};

class TreeNode : public QTreeWidgetItem
{
    public:
       QGraphicsItem * item;
       TreeNode () : item (nullptr) { }
};

#endif // TREE_H
