
/* html.h */

#ifndef HTML_H
#define HTML_H

#ifdef WEBKIT
   #include <QWebView>
#else
   #include <QWebEngineView>
#endif

#include <QVBoxLayout>
#include <QToolBar>
#include <QStatusBar>
#include <QLineEdit>

class HtmlView : public QWidget
{
   Q_OBJECT
   public:
      HtmlView (QWidget * parent, QStatusBar * statusbar_param);
      void load(QUrl url);

    private slots:
       void adjustLocation ();
       void changeLocation ();
       void setProgress (int p);

    private:
       #ifdef WEBKIT
          QWebView * view;
       #else
          QWebEngineView * view;
       #endif
       QVBoxLayout * layout;
       QToolBar * toolbar;
       QLineEdit * locationEdit;
       QStatusBar * statusbar;
};

#endif // HTML_H

