#ifndef DB_H
#define DB_H

#include <QTableView>
#include <QTreeWidget>
#include <QTextEdit>

#include <QGraphicsView>

#include <QSqlError>

class DbView : public QTableView
{
   private:
      QGraphicsScene * scene;
      QTreeWidget * tree;
      QTextEdit * info;

   private:
      void message (QSqlError err);
      void example ();

   public:
      explicit DbView (QWidget * parent,
                       QGraphicsScene * scene,
                       QTreeWidget * tree_param = NULL,
                       QTextEdit * info_param = NULL);
};

#endif // DB_H
