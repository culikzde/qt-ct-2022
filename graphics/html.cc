
/* html.cc */

#include "html.h"

#include <QToolBar>
#include <QVBoxLayout>

/* ---------------------------------------------------------------------- */

HtmlView::HtmlView (QWidget * parent, QStatusBar * statusbar_param) :
   QWidget (parent),
   view (NULL),
   layout (NULL),
   toolbar (NULL),
   locationEdit (NULL),
   statusbar (statusbar_param)
{
   toolbar = new QToolBar (this);
   #ifdef WEBKIT
      view = new QWebView (this);
      typedef QWebPage WebPage;
   #else
      view = new QWebEngineView (this);
      typedef QWebEnginePage WebPage;
   #endif

   connect (view, SIGNAL(loadProgress(int)),  SLOT (setProgress(int)));
   connect (view, SIGNAL(loadFinished(bool)), SLOT (adjustLocation()));

   locationEdit = new QLineEdit (this);
   locationEdit->setSizePolicy (QSizePolicy::Expanding, locationEdit->sizePolicy().verticalPolicy());
   connect(locationEdit, SIGNAL (returnPressed()), SLOT (changeLocation()));

   QToolBar *toolBar = new QToolBar ( "Navigation", this);
   toolBar->addAction (view->pageAction (WebPage::Back));
   toolBar->addAction (view->pageAction (WebPage::Forward));
   toolBar->addAction (view->pageAction (WebPage::Reload));
   toolBar->addAction (view->pageAction (WebPage::Stop));
   toolBar->addWidget (locationEdit);

   layout = new QVBoxLayout (this);
   layout->addWidget (toolBar);
   layout->addWidget (view);
   this->setLayout (layout);

   // load (QUrl ("http://doc.qt.io/qt-4.8"));
}

void HtmlView::load (QUrl url)
{
    locationEdit->setText (url.toString());
    view->load (url);
}

void HtmlView::adjustLocation ()
{
    locationEdit->setText (view->url().toString());
}

void HtmlView::changeLocation ()
{
    QString text = locationEdit->text();
    if (! text.startsWith ("file:") &&
        ! text.startsWith ("ftp:") &&
        ! text.startsWith ("http:") &&
        ! text.startsWith ("https:"))
    {
        text = "http:" + text;
    }
    QUrl url = QUrl (locationEdit->text());
    view->load (url);
    view->setFocus();
}

void HtmlView::setProgress (int p)
{
    if (statusbar != NULL)
        statusbar->showMessage (QString::number (p) + "%");
}

/* ---------------------------------------------------------------------- */
