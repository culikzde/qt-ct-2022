#include "demo.h"
#include "ui_demo.h"

#include <QApplication>
#include <QClipboard>

#include <QFileDialog>
#include <QMessageBox>

#include <QXmlStreamReader>
#include <QXmlStreamWriter>

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsLineItem>

#include "colorbutton.h"
#include "toolbutton.h"
#include "io.h"
#include "jsonio.h"
#include "xmlio.h"
#include "property.h"
#include "tree.h"
#include "scene.h"

#ifdef CHART
   #include "chart.h"
#endif

#ifdef VISUAL
   #include "visual.h"
#endif

#ifdef DB
   #include "db.h"
#endif

#ifdef JS
   #include "js.h"
#endif

#ifdef SOCKET
   #include "socket.h"
#endif

#ifdef DBUS
   #include "dbus.h"
#endif

#ifdef HTML
   #include "html.h"
#endif

#ifdef SVG
   #include "svg.h"
#endif

MainWindow::MainWindow (QWidget * parent) :
    QMainWindow (parent),
    ui (new Ui::MainWindow)
{
    ui->setupUi (this);

    ui->hsplitter->setStretchFactor (0, 1);
    ui->hsplitter->setStretchFactor (1, 4);
    ui->hsplitter->setStretchFactor (2, 2);

    ui->vsplitter->setStretchFactor (0, 3);
    ui->vsplitter->setStretchFactor (1, 1);

    ui->palette->removeTab (0);

    QToolBar * toolPage = new QToolBar;
    addToolButtons (toolPage);
    ui->palette->addTab (toolPage, "Tools");

    QToolBar * colorPage = new QToolBar;
    addColorButtons (colorPage);
    ui->palette->addTab (colorPage, "Colors");

    ui->palette->setCurrentWidget(toolPage);
    // ui->palette->setCurrentWidget (colorPage);

    // scene = new QGraphicsScene;
    scene = new Scene;
    ui->graphicsView->setScene (scene);
    scene->setSceneRect (0, 0, 800, 600);
    // scene->addLine (0, 0, 100, 200, QColor ("red"));

    scene->setWin (this);

    ui->tree->setScene (scene);
    ui->tree->setWin (this);

    ui->prop->setWin (this);

    ui->statusbar->showMessage (QString ("Qt ") + qVersion());

    #ifdef JS
       JsEdit * e = new JsEdit (this, ui->tree, ui->info);
       addView ("Java Script", e);
       connect (ui->actionExecuteJS, SIGNAL (triggered ()), e, SLOT (execute ()));
    #else
       ui->menuJScript->setVisible (false);
       ui->actionExecuteJS->setVisible (false);
    #endif

    #ifdef DBUS
       new Receiver (this, ui->info);
    #endif

    #ifdef SOCKET
       new SocketReceiver (this, 12345, ui->info);
    #endif

    #ifndef CHART
       ui->actionChart->setVisible (false);
       ui->actionPieChart->setVisible (false);
    #endif

    #ifndef VISUAL
       ui->actionVisualisation->setVisible (false);
    #endif

    #ifndef DB
       ui->actionDatabase->setVisible (false);
    #endif

    #ifndef HTML
       ui->actionHTML->setVisible (false);
    #endif

    #if ! defined (HTML) || ! defined (SOCKET)
       ui->actionHTML_Local_Socket ->setVisible (false);
    #endif

    #ifndef SVG
       ui->actionSVG->setVisible (false);
    #endif

    loadFile (":/data/abc.xml");
}

MainWindow::~MainWindow()
{
    delete ui;
}

/* open / save */

void MainWindow::loadFile (QString fileName, bool json)
{
    QFile f (fileName);
    if (f.open (QFile::ReadOnly))
    {
        if (json)
        {
            QByteArray code = f.readAll ();
            readJson (code, scene);
        }
        else
        {
           QXmlStreamReader r (&f);
           readXml (r, scene);
        }
        refreshTree ();
    }
    else
    {
       QMessageBox::warning (NULL, "Open File Error", "Cannot read file: " + fileName);
    }
}

void MainWindow::saveFile (QString fileName, bool json)
{
    QFile f (fileName);
    if (f.open (QFile::WriteOnly))
    {
        if (json)
        {
            QByteArray code = writeJson (scene);
            f.write (code);
        }
        else
        {
           QXmlStreamWriter w (&f);
           writeXml (w, scene);
        }
    }
    else
    {
       QMessageBox::warning (NULL, "Save File Error", "Cannot write file: " + fileName);
    }
}

const QString dir = QString ();
const QString filter = "Json files (*.json);;XML files (*.xml)";
const QString jsonFilter = "Json files";

void MainWindow::on_actionOpen_triggered ()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getOpenFileName (this, "Open file", dir, filter, &selectedFilter);
    if (fileName != "")
        // loadFile (fileName, selectedFilter.startsWith (jsonFilter));
        loadFile (fileName, fileName.endsWith (".json"));
}

void MainWindow::on_actionSave_triggered ()
{
    QString selectedFilter;
    QString fileName = QFileDialog::getSaveFileName (this, "Save file", dir, filter, &selectedFilter);
    if (fileName != "")
       // saveFile (fileName, selectedFilter.startsWith (jsonFilter));
       saveFile (fileName, fileName.endsWith (".json"));
}


/* clipboard */

void MainWindow::on_actionCopy_triggered ()
{
    QString code = "";
    QXmlStreamWriter writer (& code);
    writeBegin (writer);

    for (QGraphicsItem * item : scene->selectedItems ())
    {
        writeItem (writer, item);
    }

    writeEnd (writer);

    QMimeData * data = new QMimeData;
    data->setData (shapeFormat, code.toLatin1 ());

    QClipboard * clip = QApplication::clipboard ();
    clip->setMimeData (data);
}

void MainWindow::on_actionPaste_triggered ()
{
    QClipboard * clip = QApplication::clipboard ();
    const QMimeData * data = clip->mimeData ();
    if (data->hasFormat (shapeFormat))
    {
       QString code = data->data (shapeFormat);
       QXmlStreamReader reader (code);
       readXml (reader, scene);
    }
    refreshTree ();
}

/* tree */

void MainWindow::refreshTree ()
{
    ui->tree->displayTree ();
}

void MainWindow::refreshProperties (QGraphicsItem * item)
{
    ui->prop->displayProperties (item);
}

void MainWindow::refreshTreeName (QGraphicsItem * item, QString name)
{
    ui->tree->renameTreeItem (item, name);
}

void MainWindow::refreshNewTreeItem (QGraphicsItem * item)
{
   ui->tree->addTreeItem (item);
}

void MainWindow::on_tree_itemDoubleClicked (QTreeWidgetItem * node, int column)
{
    TreeNode * tree_node = dynamic_cast <TreeNode *> (node);
    if (tree_node != nullptr && tree_node->item != nullptr)
       refreshProperties (tree_node->item);
    else
       refreshProperties (nullptr);
}

void MainWindow::on_tree_itemSelectionChanged ()
{
    QGraphicsItem * item = nullptr;
    QList <QTreeWidgetItem * > list = ui->tree->selectedItems ();
    if (list.count () == 1)
    {
       TreeNode * node = dynamic_cast < TreeNode * > (list[0]);
       if (node != nullptr)
          item = node->item;
    }
    refreshProperties (item);
}

/* run */

void MainWindow::on_actionRun_triggered ()
{
   QGraphicsRectItem * block = new QGraphicsRectItem;
   block->setRect (0, 0, 200, 160);
   block->setPos (100, 100);
   block->setPen (QColor ("orange"));
   block->setBrush (QColor ("yellow"));
   block->setToolTip ("block");
   block->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
   scene->addItem (block);

   for (int i = 1; i <= 2; i++)
   {
       QGraphicsEllipseItem * item = new QGraphicsEllipseItem;
       item->setRect (0, 0, 40, 40);
       item->setPos (40 + (i-1) * 80, 40);
       item->setPen (QColor ("blue"));
       item->setBrush (QColor ("cornflowerblue"));
       item->setToolTip ("item" + QString::number (i));
       item->setFlags (QGraphicsItem::ItemIsMovable | QGraphicsEllipseItem::ItemIsSelectable);
       item->setParentItem (block);
   }

   refreshTree ();
}

/* view */

void MainWindow::addView (QString title, QWidget * widget)
{
    widget->setParent (this);
    ui->tabs->addTab (widget, title);
    ui->tabs->setCurrentWidget (widget);
}

void MainWindow::on_actionChart_triggered()
{
#ifdef CHART
    addView ("Chart", chartExample (scene));
#endif
}

void MainWindow::on_actionPieChart_triggered()
{
#ifdef CHART
     addView ("Pie Chart", pieChartExample (scene));
#endif
}

void MainWindow::on_actionVisualisation_triggered()
{
#ifdef VISUAL
    addView ("Bar 3D", dataVisualizationExample ());
#endif
}

void MainWindow::on_actionDatabase_triggered()
{
#ifdef DB
    addView ("Database", new DbView (this, scene, ui->tree, ui->info));
#endif
}

void MainWindow::on_actionSVG_triggered()
{
#ifdef SVG
    addView ("SVG", new SvgView (this));
#endif
}

void MainWindow::on_actionHTML_triggered()
{
#ifdef HTML
    HtmlView * view = new HtmlView (this, ui->statusbar);
    addView ("HTML", view);
    view->load (QUrl ("http://doc.qt.io/qt-4.8"));
#endif
}

void MainWindow::on_actionHTML_Local_Socket_triggered()
{
#ifdef HTML
    HtmlView * view = new HtmlView (this, ui->statusbar);
    addView ("HTML Local Socket", view);
    view->load (QUrl ("http://localhost:1234"));
#endif
}

void MainWindow::on_actionClear_triggered()
{
    for (QGraphicsItem * item : scene->items (Qt::AscendingOrder))
    {
        scene->removeItem (item);
        delete item;
    }
    refreshTree ();
}

void MainWindow::on_actionQuit_triggered ()
{
   close ();
}

int main (int argc, char * argv [])
{
    QApplication a (argc, argv);
    MainWindow w;
    w.show ();
    return a.exec ();
}


