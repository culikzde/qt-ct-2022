#ifndef COLORBUTTON_H
#define COLORBUTTON_H

#include <QToolButton>
#include <QToolBar>

#include <QMimeData>
#include <QDrag>
#include <QMouseEvent>

class ColorButton : public QToolButton
{
private:
   QColor color;
   QString name;
   QPixmap getPixmap ();
protected:
    void mousePressEvent (QMouseEvent *event);
public:
    ColorButton (QColor p_color, QString p_name = "");
};

void addColorButtons (QToolBar * page);

#endif // COLORBUTTON_H
